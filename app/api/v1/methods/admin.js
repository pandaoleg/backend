class APIAdmin {
  constructor(lib) {
    this.lib = lib;
  }

  /**
   * Method for getting the user information. Also can get only one user information by id
   */
  getUsersInformation(data, response, adminInfo) {
    const { selectQuery } = this.lib.helper.schema;
    let query = `SELECT ${selectQuery.user}, ${selectQuery.permission} FROM accounts ${selectQuery.joinForPermissionName}`;
    if (data.param.id || data.param.id === 0) query += ' WHERE Code=?';

    this.lib.db.prepareQueryToDatabaseWithReturns(query, data.param.id || data.param.id === 0 ? [data.param.id] : []).then(users => {
      if (users.length === 0) response.status(204).send();
      else {
        users = Array.isArray(users) ? users.map(user => this.lib.helper.schema.getPublicUserAuthInfo(user)) : this.lib.helper.schema.getPublicUserAuthInfo(users);
        if (Array.isArray(users)) users = users.filter(user => user.login !== adminInfo.Username);
        response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(users));
      }
    }).catch(err => {
      this.lib.logger.error(`[PM-Admin] An error occured in getting user's information: ${err}`);
      response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
    });
  }

  setUpdateChangelog(data, response) {

  }

  /**
   * Method for changing the role of user
   */
  changeUsersRole(data, response, adminInfo) {
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['userId', 'newRoleName'], data.param))
      return response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
    else if (adminInfo.ID === data.param.userId)
      return response.status(412).send(this.lib.apiMessage.ERROR.CANNOT_CHANGE_ROLE_ITSELF());

    // validate role by name
    const { userId, newRoleName } = data.param;
    this.lib.db.prepareQueryToDatabaseWithReturns('SELECT Count(*) as `Count`, ID from permission WHERE Name=?', [newRoleName]).then(roleResult => {
      if (roleResult[0].Count === 0) return response.status(400).send(this.lib.apiMessage.ERROR.WRONG_ROLE_NAME());

      this.lib.db.prepareQueryToDatabaseWithoutReturns('UPDATE accounts SET IDPermission=? WHERE Code=?', [roleResult[0].ID, userId]).then(() => {
        response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE());
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Method for deleting an account
   */
  deleteAccount(data, response) {
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['userId'], data.param)) 
      return response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());

    this.lib.helper.schema.getUserById(data.param.userId).then(user => {
      // First we need to delete account's folder on server
      const rmdir = require('rmdir');
      rmdir(this.lib.helper.path.getUserFolderPath(user.Username), (err, dirs, files) => {
        if (err) {
          this.lib.logger.error(`[PM-Admin] An error occured on deleting account: ${err}`);
          response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
        } else {
          // Then remove account's data from database
          let waiting = [];
          
          // Remove account data
          waiting.push(new Promise((resolve, reject) => {
            const db = {
              query: 'DELETE FROM accounts WHERE Code=?',
              param: [user.ID]
            };
  
            this.lib.db.prepareQueryToDatabaseWithoutReturns(db.query, db.param).then(() => {
              resolve();
            }).catch(err => {
              reject(err);
            });
          }));

          // Remove review data
          waiting.push(new Promise((resolve, reject) => {
            const db = {
              query: 'DELETE FROM review WHERE UserID=?',
              param: [user.ID]
            };
  
            this.lib.db.prepareQueryToDatabaseWithoutReturns(db.query, db.param).then(() => {
              resolve();
            }).catch(err => {
              reject(err);
            });
          }));

          // Remove avatar
          waiting.push(new Promise((resolve, reject) => {
            this.lib.fs.unlink(`${this.lib.helper.path.getAssetsFolderPath()}/avatars/${user.Token}.jpg`, (err => {
              if (err) {
                this.lib.logger.error(`[PM-Admin] An error occured on deleting account (remove avatar): ${err}`);
                reject();
              }

              resolve();
            }));
          }));

          Promise.all(waiting).then(() => {
            response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE());
          }).catch(err => {
            this.lib.logger.error(`[PM-Admin] An error occured on deleting account (waiting promise): ${err}`);
            response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR(err));
          });
        }
      });
    }).catch(err => {
      if (err.isQuerySuccess) response.status(err.status || 520).send(this.lib.apiMessage.ERROR.CANNOT_FIND_USER_ID());
      else response.status(err.status || 520).send(err.message);
    });
  }
}

module.exports = (lib) => new APIAdmin(lib);
