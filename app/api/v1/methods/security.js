class APISecurity {
  constructor(lib) {
    this.lib = lib;
  }
  
  /**
   * Method for authorizing the user by login and password
   * @param { Object } params An object with params
  */
  authorizeByLoginAndPassword(data, response) {
    const params = data.param;
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['login', 'password'], params)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.helper.schema.getUserByLogin(params.login).then(user => {
      // comparing a password
      this.lib.bcrypt.compare(params.password, user.Password, (err, result) => {
        if (err || !result) response.status(403).send(this.lib.apiMessage.ERROR.WRONG_USER_CREDITS());
        else {
          this._isAccountFolderIsExists(user.Username).then(status => {
            // If account folder is not exists
            // Then we need to create this folder and other files
            if (!status) {
              this._createAccountFolderAndFiles({login: user.Username, token: user.Token}).then(() => {
                response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(this.lib.helper.schema.getUserAuthInfo(user)));
              }).catch(err => {
                response.status(520).send(err);
              });
            } else response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(this.lib.helper.schema.getUserAuthInfo(user)));
          });
          
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Method for auth with google token
   */
  authByGoogleApi(data, response) {
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['token'], data.param)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    /**
     * Validating a Google id_token by calling google api
     * This step is just for to be sure that we got token from panda-music only
     */
    const O2AuthClient = this.lib.google.OAuth2Client;
    const client = new O2AuthClient(this.lib.config.googleClientId);
    client.verifyIdToken({idToken: data.param.token, audience: this.lib.config.googleClientId}).then(ticket => {
      const payload = ticket.getPayload();
      const userId = payload.sub;

      // Then check for existing a link of google with account by email
      // Send auth data only if user has valid user id and it is linked in database
      this.lib.helper.schema.getUserByEmail(payload.email).then(user => {
        // If user still doesn't have a google id in database
        // Then we need to update account data
        if (!user.GoogleID) {
          this._linkGoogleAccount(userId, user.ID).then(() => {
            response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(this.lib.helper.schema.getUserAuthInfo(user)));
          }).catch(err => {
            this.lib.logger.error(`[PM-API] An error occured on linking google account: ${err.message}`);
            response.status(err.status || 520).send(err.message);
          });
        }
        else if (user.GoogleID !== userId) response.status(403).send(this.lib.apiMessage.ERROR.AUTH_GOOGLEID_IS_NOT_RIGHT());
        else response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(this.lib.helper.schema.getUserAuthInfo(user)));
      }).catch(err => {
        // If err hash property isQuerySuccess
        // Then we need to create this account first
        if (err.isQuerySuccess) {
          this.registerAccount({
            key: '',
            param: {
              login: payload.email.split('@')[0],
              password: 'google-password',
              isGoogle: true,
              email: payload.email
            }
          }, response);
        } else response.status(err.status || 520).send(err.message);
      });
    }).catch(err => {
      this.lib.logger.error(`[PM-API] An error occured on sign-in with google: ${err.message.split(':')[0]}`);
      response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR(err.message.split(':')[0]));
    });
  }

  /**
   * Method for checking user right's by role name
   * @param { Object } params An object with params
   * */
  isUserIsHaveRightsByRoleName(data, response) {
    const params = data.param;
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['roleName'], params)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.helper.schema.getUserByToken(data.key, 'Token').then(user => {
      let result = user.PermissionName === 'Администратор' ? true : user.PermissionName === params.roleName;
      response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE({isUserHaveRights: result}));
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Method for make link on google account to database
   * @param { string } googleId A google account id 
   */
  _linkGoogleAccount(googleId, userId) {
    const db = {
      query: 'UPDATE accounts SET GoogleID=? WHERE Code=?',
      param: [googleId, userId]
    };

    return this.lib.db.prepareQueryToDatabaseWithoutReturns(db.query, db.param);
  }

  /**
   * Method for checking exists user folder or not
   * @returns { Promise } Return a promise with 1 boolean param. True if folder is exists
   */
  _isAccountFolderIsExists(login) {
    return new Promise((resolve, reject) => {
      this.lib.fs.stat(`./app/users/${login}`, (err, stat) => {
        resolve(err ? false : true);
      })
    });
  }

  /** 
   * Method for creating an user folder and file's
   * @param { Object } user An user object with info from database
   * @param { string } user.login An user's login
   * @param { string } user.token An user's password
  */
  _createAccountFolderAndFiles(user) {
    return new Promise((resolve, reject) => {
      let waitCreatingFolders = [];
      const settings = {
        volume: 1,
        equalizer: {
          enabled: true,
          band: ["0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0.0", "0.0"]
        }
      };
      const playlist = { custom: [] };

      // create account folder
      waitCreatingFolders.push(new Promise((resolve, reject) => {
        this.lib.fs.mkdir(this.lib.helper.path.getUserFolderPath(user.login), (err) => {
          if (err) reject(err);
          else resolve();
        });
      }));

      // create tracks folder
      waitCreatingFolders.push(new Promise((resolve, reject) => {
        this.lib.fs.mkdir(`${this.lib.helper.path.getUserFolderPath(user.login)}/tracks`, (err) => {
          if (err) reject(err);
          else resolve();
        });
      }));

      // create cache folder
      waitCreatingFolders.push(new Promise((resolve, reject) => {
        this.lib.fs.mkdir(`${this.lib.helper.path.getUserFolderPath(user.login)}/cache`, (err) => {
          if (err) reject(err);
          else {
            // create cache image folder
            waitCreatingFolders.push(new Promise((resolve, reject) => {
              this.lib.fs.mkdir(`${this.lib.helper.path.getUserFolderPath(user.login)}/cache/cover`, (err) => {
                if (err) reject(err);
                else resolve();
              });
            }));
            
            resolve();
          } 
        });
      }));
  
      // create a file with setting's
      waitCreatingFolders.push(new Promise((resolve, reject) => {
        this.lib.fs.writeFile(`${this.lib.helper.path.getUserFolderPath(user.login)}/setting.json`, JSON.stringify(settings), (err) => {
          if (err) reject(err);
          else resolve();
        });
      }));
  
      // create a file with custom playlist's 
      waitCreatingFolders.push(new Promise((resolve, reject) => {
        this.lib.fs.writeFile(`${this.lib.helper.path.getUserFolderPath(user.login)}/playlist.json`, JSON.stringify(playlist), (err) => {
          if (err) reject(err);
          else resolve();
        });
      }));
  
      // copy default avatar to avatars folder
      waitCreatingFolders.push(new Promise((resolve, reject) => {
        const paths = {
          defaultAvatar: `${this.lib.helper.path.getAssetsFolderPath()}/default_avatar.jpg`,
          userAvatar: `${this.lib.helper.path.getAvatarFolderPath()}/${user.token}.jpg`
        };
  
        this.lib.fs.copyFile(paths.defaultAvatar, paths.userAvatar, (err) => {
          if (err) reject(err);
          else resolve();
        });
      }));

      Promise.all(waitCreatingFolders).then(() => {
        resolve();
      }).catch(err => {
        this.lib.logger.error(`[PM-API] An error occured on creating user files/folder: ${err}`);
        reject(this.lib.apiMessage.ERROR.SERVER_ERROR())
      })
    });
  }

  /**
   * Method for register an account
  */
  async registerAccount(data, response) {
    /**
     * Method for validating an user input for antihacking
     */
    const validateUserInput = () => {
      if (!this.lib.validator.isEmail(params.email)) 
        return { error: true, message: this.lib.apiMessage.ERROR.REGISTER_WRONG_EMAIL() };
      if (!this.lib.helper.apiValidator.validateLoginRegex(params.login)) 
        return { error: true, message: this.lib.apiMessage.ERROR.REGISTER_WRONG_LOGIN() };
      return true;
    };

    const params = data.param;
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['login', 'password', 'email'], params)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    const validator = validateUserInput();
    if (validator.error) {
      response.status(400).send(validator.message);
      return;
    }

    if (params.isGoogle) params.password = new this.lib.tokenGenerator().generate()

    // Create an object with user info 
    // for comfortable uses
    const user = {
      login: params.login,
      email: params.email,
      password: await this.lib.bcrypt.hash(params.password, 10),
      IDPermission: 3,
      token: new this.lib.tokenGenerator(256, this.lib.tokenGenerator.BASE62).generate(),
    };

    // Checking for existing user
    this.lib.helper.schema.isUserExists(user.login, user.email).then(isExists => {
      if (isExists) {
        response.status(409).send(this.lib.apiMessage.ERROR.REGISTER_USER_EXISTS());
        return;
      }

      const query = "INSERT INTO `accounts` (`Username`, `E-mail`, `Password`, `DateOfCreating`, `Token`) VALUES (?, ?, ?, Now(), ?)",
        values = [user.login, user.email, user.password, user.token];

      this._createAccountFolderAndFiles(user).then(() => {
        // Insert user data to database
        // (register an account)
        this.lib.db.prepareQueryToDatabaseWithoutReturns(query, values).then(() => {
          // If creating account is successed
          // Then we can just send auth info to client
          if (data.param.isGoogle) {
            this.lib.helper.schema.getUserByLogin(user.login).then(user => {
              response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(this.lib.helper.schema.getUserAuthInfo(user)));
            }).catch(err => {
              response.status(err.status).send(err.message);
            });
          } else response.send(this.lib.apiMessage.SUCCESS.REGISTER_SUCCESS());
        }).catch(queryError => {
          // Remove user's folder
          this.lib.removeDir(this.lib.helper.path.getUserFolderPath(user.login), (err) => {
            if (err) this.lib.logger.error(`[PM-API] An error occured on removing user folder: ${err}`);

            // Remove user's avatar
            this.lib.fs.unlink(`${this.lib.helper.path.getAvatarFolderPath()}/${user.token}.jpg`, (err) => {
              if (err) this.lib.logger.error(`[PM-API] An error occured on removing user avatar: ${err}`);
              response.send(queryError.message);
            });
          });
        });
      }).catch(err => {
        response.status(520).send(err);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = (lib) => new APISecurity(lib);