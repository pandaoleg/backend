class APITrack {
  constructor(lib) {
    this.lib = lib;
  }

  /**
   * Method for uploading a track (or tracks)
   */
  uploadTracks(request, response, data) {
    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      if (!request.files) return response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());

      const keys = Object.keys(request.files);
      const countOfFiles = keys.length;
      let waitingUpload = [];

      for (let i = 0; i < countOfFiles; i++) {
        waitingUpload.push(new Promise((resolve, reject) => {
          const currentFile = request.files[keys[i]];
          if (!currentFile.mimetype.includes('audio')) resolve({status: 'error', message: 'Неверный формат файла!'});
          const uploadFilePath = `./app/users/${user.Username}/tracks/${currentFile.name}`;
          currentFile.mv(uploadFilePath, err => {
            if (err) resolve({status: 'error', message: 'Произошла ошибка на стороне сервера'});
            else {
              // parse metadata of track
              this.lib.id3Reader.parseFile(uploadFilePath, { duration: true, native: true }).then(data => {
                data.filePath = uploadFilePath;
                let info = this.lib.helper.schema.getTrackInfo(data), waiting = [];

                // Get track' hash
                waiting.push(new Promise((resolve, reject) => {
                  this.lib.md5(data.filePath, (err, hash) => {
                    if (err) reject({status: 'error', musicData: err});
              
                    info.hash = hash;
                    info.fileName = currentFile.name;

                    // If we have an image
                    // Than we need to save this
                    if (data.common.picture) {
                      let coverPath = `./app/users/${user.Username}/cache/cover/${hash}.jpeg`;
                      let stream = this.lib.fs.createWriteStream(coverPath);
                      stream.write(new Buffer(data.common.picture[0].data));
                      stream.end();
                      info.isDefaultThumbnail = false;
                    } else info.isDefaultThumbnail = true;

                    resolve({status: 'success', musicData: info});
                  });
                }));

                Promise.all(waiting).then(data => {
                  resolve(data[0]);
                }).catch(err => {
                  reject(err);
                });
              }).catch(err => {
                reject(err);
              });
            }
          });
        }));
      }

      // Waiting untill all process of upload a track(s) completed
      Promise.all(waitingUpload).then(filesUploadStatus => {
        let waiting = [];
        for (let trackInfo of filesUploadStatus) {
          // If status of saving and getting info is success
          // Then we need to save cache to specific file with that info
          waiting.push(new Promise((resolve, reject) => {
            if (trackInfo.status === 'success') {
              this.lib.fs.writeFile(`./app/users/${user.Username}/cache/${trackInfo.musicData.hash}.json`, JSON.stringify(trackInfo.musicData), err => {
                if (err) this.lib.logger.info(`[API-Track] An error occured on writing a cache to file: ${err}`);
                resolve(trackInfo);
              });
            } else resolve(trackInfo);
          }));
        }

        return Promise.all(waiting);
      }).then(data => {
        response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(data));
      }).catch(err => {
        response.status(520).send(err);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Method for getting track's cache from files
   */
  getMusicCache(data, response) {
    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      const path = `${this.lib.helper.path.getUserFolderPath(user.Username)}/cache`;
      this.lib.fs.readdir(path, (err, files) => {
        if (err) {
          this.lib.logger.info(`[API-Track] An error occured on getting files in cache method: ${err}`);
          return response.status(520).send(this.lib.apiMessage.ERROR.SERVER_ERROR());
        } else {
          let waiting = [];
          for (let i = 0; i < files.length; i++) {
            // Read all files
            // That includes a cache info about tracks
            waiting.push(new Promise((resolve, reject) => {
              this.lib.fs.stat(`${path}/${files[i]}`, (err, stats) => {
                if (err) {
                  this.lib.logger.info(`[API-Track] An error occured on getting status in cache method: ${err}`);
                  reject(this.lib.apiMessage.ERROR.SERVER_ERROR());
                } else {
                  // Read only if it's a file
                  if (stats.isFile()) {
                    this.lib.fs.readFile(`${path}/${files[i]}`, 'utf-8', (err, data) => {
                      if (err) {
                        this.lib.logger.info(`[API-Track] An error occured on getting status in cache method: ${err}`);
                        reject(this.lib.apiMessage.ERROR.SERVER_ERROR());
                      } else resolve(JSON.parse(data));
                    });
                  } else resolve();
                }
              });
            }));
          }

          // Wait until readining file is complete
          Promise.all(waiting).then(data => {
            const collectUrls = (tracks) => {
              for (let i = 0; i < tracks.length; i++) {
                // collect thumbnail info
                tracks[i].thumbnail = tracks[i].isDefaultThumbnail ? 
                  this.lib.helper.url.getThumbnailDefault() : this.lib.helper.url.getThumbnailCache(tracks[i].hash, user.Username);
                tracks[i].isDefaultThumbnail = undefined;

                // collect track url
                tracks[i].fileUrl = this.lib.helper.url.getTrackUrl(tracks[i].fileName, user.Username);
                tracks[i].fileName = undefined;
              }

              return tracks;
            };

            const collectAlbumInfo = (tracks) => {
              let albums = [];

              for (let i = 0; i < tracks.length; i++) {
                let index = albums.findIndex(album => album.name === tracks[i].album);
                if (index === -1) albums.push({ name: tracks[i].album, index: [i] });
                else albums[index].index.push(i);
              }

              return albums;
            };

            data = data.filter(info => info !== undefined);
            data = collectUrls(data);
            response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(Object.assign({}, {tracks: data, albums: collectAlbumInfo(data)})));
          }).catch(err => {
            response.status(520).send(err);
          });
        }
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  deleteTrack(data, response) {
    
  }

  /**
   * Method for recaching tracks
   */
  recacheTracks(response) {

  }
}

module.exports = (lib) => new APITrack(lib);
