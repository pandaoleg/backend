class APIUser {
  constructor(lib) {
    this.lib = lib;
  }

  /**
   * Method for updating an user's public info (e.g. full name and etc)
   */
  updateUserPublicInformation(data, response) {
    const validateUserInput = () => {
      if (!this.lib.helper.apiValidator.validateCyrilicRegex(data.param.name) || !this.lib.helper.apiValidator.validateCyrilicRegex(data.param.surname))
        return { error: true, message: this.lib.apiMessage.ERROR.REGISTER_WRONG_CYRILIC() };
      return true;
    };

    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['name', 'surname'], data.param))
      return response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());

    // validate cyrilic format
    const validateNameAndSurname = validateUserInput();
    if (validateNameAndSurname.error) return response.status(400).send(validateNameAndSurname.message);

    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      const db = {
        query: 'UPDATE accounts SET Name=?, Surname=? WHERE Code=?',
        param: [
          data.param.name,
          data.param.surname,
          user.Code
        ]
      };

      this.lib.db.prepareQueryToDatabaseWithoutReturns(db.query, db.param).then(() => {
        user.FirstName = data.param.name;
        user.Surname = data.param.surname;
        response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(this.lib.helper.schema.getUserAuthInfo(user)));
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Method for changing an user's password with new password
   */
  updateUserPassword(data, response) {
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['newPassword'], data.param)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      const db = {
        query: 'UPDATE accounts SET Password=?, Token=? WHERE Code=?',
        param: [
          data.param.newPassword,
          new this.lib.tokenGenerator(128, this.lib.tokenGenerator.BASE62).generate(),
          user.Code
        ]
      };

      this.lib.db.prepareQueryToDatabaseWithoutReturns(db.query, db.param).then(() => {
        user.Password = data.param.newPassword;
        response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE(this.lib.helper.schema.getUserAuthInfo(user)));
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }

  /**
   * Method for make a link with google account's
   */
  linkAccountToGoogle(data, response) {
    if (!this.lib.helper.apiValidator.validateParamsDependingOnMethod(['id'], data.param)) {
      response.status(400).send(this.lib.apiMessage.ERROR.WRONG_PARAMS());
      return;
    }

    this.lib.helper.schema.getUserByToken(data.key).then(user => {
      const db = {
        query: 'UPDATE accounts SET GoogleID=? WHERE Code=?',
        param: [
          data.param.id,
          user.Code
        ]
      };

      this.lib.db.prepareQueryToDatabaseWithoutReturns(db.query, db.param).then(() => {
        response.send(this.lib.apiMessage.SUCCESS.EMPTY_MESSAGE());
      }).catch(err => {
        response.status(err.status).send(err.message);
      });
    }).catch(err => {
      response.status(err.status).send(err.message);
    });
  }
}

module.exports = (lib) => new APIUser(lib);
