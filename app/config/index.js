class Config {
  constructor() {
    this.database = require('./dev.json');
    this.host = `http://localhost:8080`;
    this.googleClientId = '601378698565-pimt6miafsngp0aovq98con18t0fg44h.apps.googleusercontent.com'
  }
}

module.exports = new Config();
