class Schema {
  constructor(lib) {
    this.lib = lib;
    this.selectQuery = {
      user: 'accounts.Code as ID, accounts.Username, accounts.IDPermission, accounts.`E-mail`, accounts.Name as FirstName, accounts.Surname, accounts.Token, accounts.Password, accounts.GoogleID',
      permission: 'permission.Name as PermissionName, permission.Description as PermissionDescription',
      joinForPermissionName: 'INNER JOIN permission on accounts.IDPermission = permission.ID'
    }
  }

  getUserBy(value, conditionField, additionalCondition = '') {
    return new Promise((resolve, reject) => {
      if (!value) reject({message: this.lib.apiMessage.ERROR.WRONG_PARAMS()});

      const query = `SELECT ${this.selectQuery.user}, ${this.selectQuery.permission} FROM accounts ${this.selectQuery.joinForPermissionName} WHERE \`${conditionField}\`=? ${additionalCondition}`;
      this.lib.db.prepareQueryToDatabaseWithReturns(query, Array.isArray(value) ? value : [value]).then(data => {
        if (data.length === 0) reject({message: this.lib.apiMessage.ERROR.WRONG_USER_CREDITS(), isQuerySuccess: true, status: 403});
        resolve(data[0]);
      }).catch(err => {
        reject(err);
      });
    });
  }
  
  getUserByLogin(login) {
    return this.getUserBy(login, 'Username');
  }

  getUserByToken(token) {
    return this.getUserBy(token, 'Token');
  }

  getUserByEmail(email) {
    return this.getUserBy(email, 'E-mail');
  }

  getUserById(id) {
    return this.getUserBy(id, 'Code');
  }

  getUserByTokenAndValidateByRole(token, roleId) {
    return new Promise((resolve, reject) => {
      this.getUserBy([token], 'Token').then(user => {
        if (user.IDPermission !== roleId) reject({message: this.lib.apiMessage.ERROR.PERMISSION_DENIED(), status: 403});
        resolve(user);
      }).catch(err => {
        reject(err);
      })
    });
  }

  isUserExists(login, email) {
    return new Promise((resolve, reject) => {
      this.getUserBy([login, email], 'Username', 'OR `E-mail`=?').then(user => {
        resolve(!!user);
      }).catch(err => {
        if (err.isQuerySuccess) resolve(false);
        reject(err);
      })
    });
  }

  /**
   * Method for getting simplify user info
   * @param { Object } user An user object
   */
  getUserAuthInfo(user) {
    return {
      id: user.ID,
      login: user.Username,
      token: user.Token,
      avatar: this.lib.helper.url.getUserAvatar(user.Token),
      name: {
        name: user.FirstName,
        surname: user.Surname
      },
      email: user['E-mail'],
      permission: {
        name: user.PermissionName,
        description: user.PermissionDescription
      },
      setting: require(`../users/${user.Username}/setting.json`),
    }
  }

  /**
   * Method for getting simplify public user info
   * @param { Object } user An user object
   */
  getPublicUserAuthInfo(user) {
    return {
      id: user.ID,
      login: user.Username,
      name: {
        name: user.FirstName,
        surname: user.Surname
      },
      email: user['E-mail'],
      permission: {
        name: user.PermissionName,
      },
      dateOfCreating: user.DateOfCreating
    }
  }

  /**
   * Method for getting simplify track info
   * @param { Object } track A track object with information
   */
  getTrackInfo(track) {
    let trackObject = {
      bitrate: track.format.bitrate / 1000,
      duration: track.format.duration,
      artist: track.common.artist || 'unknown',
      title: track.common.title || this.lib.path.parse(track.filePath).name,
      album: track.common.album || 'Остальное'
    };
    
    return trackObject;
  }
}

module.exports = (lib) => new Schema(lib);