const mysql = require('mysql');

class Database {
  constructor(lib) {
    const connection = mysql.createConnection(lib.config.database);
    this.lib = lib;

    this.getConnection = () => {
      return connection;
    };
  }

  /**
   * Method for connect to database
   * @returns { Promise } Return a promise. then => success connection
  */
  connect() {
    return new Promise((resolve, reject) => {
      this.getConnection().connect(err => {
        if (err) reject(err);
        resolve();
      });
    });
  }

  /**
   * Method for send a prepared query (with ?) with return a result
   * @param { string } query A MySQL query
   * @param { array } values An array with data to be replaced by ? mark
  */
  prepareQueryToDatabaseWithReturns(query, values) {
    return new Promise((resolve, reject) => {
      // validate '?' and len of values
      if (query.split('?').length - 1 !== values.length) 
        reject({status: 500, message: this.lib.apiMessage.ERROR.SERVER_ERROR('Неправильный запрос к бд')})

      this.getConnection().query({
        sql: query,
        values: values
      }, (err, result, fields) => {
        if (err) reject({status: 500, message: this.lib.apiMessage.ERROR.SERVER_ERROR(err)});
        resolve(result);
      })
    });
  }

  /**
   * Method for send a prepared query (with ?)
   * @param { string } query A MySQL query
   * @param { array } values An array with data to be replaced by ? mark
  */
 prepareQueryToDatabaseWithoutReturns(query, values) {
  return new Promise((resolve, reject) => {
    // validate '?' and len of values
    if (query.split('?').length - 1 !== values.length)
      reject({status: 500, message: this.lib.apiMessage.ERROR.SERVER_ERROR('Неправильный запрос к бд')})

    this.getConnection().query({
      sql: query,
      values: values
    }, (err, result, fields) => {
      if (err) reject({status: 500, message: this.lib.apiMessage.ERROR.SERVER_ERROR(err)});
      resolve();
    })
  });
}
}

module.exports = (lib) => new Database(lib);
