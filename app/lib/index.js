module.exports = {
	helper: require('../helpers'),
	apiMessage: require('../message'),
	logger: require('./logger'),
	db: require('./database'),
	config: require('../config'),
	bcrypt: require('bcrypt'),
	tokenGenerator: require('uuid-token-generator'),
	validator: require('validator'),
	fs: require('fs'),
	removeDir: require('rimraf'),
	id3Reader: require('music-metadata'),
	path: require('path'),
	md5: require('md5-file'),
	uuid: require('uuid/v4'),
	groupArray: require('group-array'),
	google: require('google-auth-library')
};