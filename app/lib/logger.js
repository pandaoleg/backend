const winston = require('winston');

const logger = new (winston.Logger)({
  level: 'debug',
  transports: [
    new (winston.transports.File)({ filename: 'logs/error.log', level: 'error' }),
		new (winston.transports.Console)({
			timestamp: true,
			colorize: true
		})
  ],
  
  colors: [{
		info: 'blue',
		warn: 'yellow',
		error: 'red'
  }]
});

module.exports = logger;
