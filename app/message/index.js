module.exports = { 
  CREATOR: require('./creator'), 
  SUCCESS: require('./success'),
  ERROR: require('./error'),
  WARNING: require('./warning')
};