const creator = require('./creator');

class SUCCESS {
  static EMPTY_MESSAGE(data = []) {
    return creator.createSuccessMessage(``, data);
  }

  static REGISTER_SUCCESS() {
    return creator.createSuccessMessage(`Вы успешно зарегистрировались!`);
  }
}

module.exports = SUCCESS;